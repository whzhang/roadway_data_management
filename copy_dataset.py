import arcpy
import traceback
from roadway_data_management import copy_dataset,start_stop_service,disconnect_user,allow_database_connection

def copy():
    # input parameters
    source_db = str(arcpy.GetParameter(0))
    target_db = str(arcpy.GetParameter(1))
    wildcards = str(arcpy.GetParameter(2))
    server_name = str(arcpy.GetParameter(3))
    user_name = str(arcpy.GetParameter(4))
    password = str(arcpy.GetParameter(5))
    folder_name = str(arcpy.GetParameter(6))
    services = str(arcpy.GetParameter(7))

    try:
        # parse user input wildcards
        wildcards = wildcards.split(",")

        # input test
        # arcpy.AddMessage(source_db)
        # arcpy.AddMessage(target_db)
        # for wildcard in wildcards:
        #     arcpy.AddMessage(wildcard)
        # arcpy.AddMessage(server_name)
        # arcpy.AddMessage(user_name)
        # arcpy.AddMessage(password)
        # arcpy.AddMessage(folder_name)
        # arcpy.AddMessage(services)


        # Stop all services connecting to the target workspace
        if services == "ALL":
            if not start_stop_service(server_name,user_name,password,folder_name,all=True,action="STOP"):
                return
        else:
            services = services.split(",")
            for service in services:
                if not start_stop_service(server_name,user_name,password,folder_name,all=False,service_name=service,action="STOP"):
                    return
        arcpy.AddMessage("Stopped services...")

        # disconnect all users connecting to the target workspace and disallow database connection
        if allow_database_connection(target_db,allow=False) and disconnect_user(target_db,all=True):
            # migrate datasets
            copy_dataset(source_db,target_db,wildcards)
            arcpy.AddMessage("Copy datasets success!")

        # Start all services connecting to the target workspace
        if services == "ALL":
            start_stop_service(server_name,user_name,password,folder_name,all=True,action="START")
        else:
            for service in services:
                start_stop_service(server_name,user_name,password,folder_name,all=False,service_name=service,action="START")
        arcpy.AddMessage("Restarted services...")

        # Allow database connection
        allow_database_connection(target_db,allow=False)

        arcpy.AddMessage("Done!")

        return
    except Exception,ex:
        if services == "ALL":
            start_stop_service(server_name,user_name,password,folder_name,all=True,action="START")
        else:
            for service in services:
                start_stop_service(server_name,user_name,password,folder_name,all=False,service_name=service,action="START")
        arcpy.AddMessage(traceback.format_exc())


if __name__ == "__main__":
    copy()