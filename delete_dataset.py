import arcpy
import traceback
from roadway_data_management import delete_dataset,delete_domain,start_stop_service,disconnect_user,allow_database_connection


def delete():
    # input parameters
    db = str(arcpy.GetParameter(0))
    wildcards = str(arcpy.GetParameter(1))
    domains = str(arcpy.GetParameter(2))
    server_name = str(arcpy.GetParameter(3))
    user_name = str(arcpy.GetParameter(4))
    password = str(arcpy.GetParameter(5))
    folder_name = str(arcpy.GetParameter(6))
    services = str(arcpy.GetParameter(7))

    try:
        # parse user input wildcards
        wildcards = wildcards.split(",")

        # parse user input domains
        domains = domains.split(",")

        # Stop all services connecting to the target workspace
        if services == "ALL":
            if not start_stop_service(server_name,user_name,password,folder_name,all=True,action="STOP"):
                return
        else:
            services = services.split(",")
            for service in services:
                if not start_stop_service(server_name,user_name,password,folder_name,all=False,service_name=service,action="STOP"):
                    return
        arcpy.AddMessage("Stopped services...")

        # disconnect all users connecting to the target workspace and disallow database connection
        if allow_database_connection(db,allow=False) and disconnect_user(db,all=True):
            # migrate datasets
            delete_dataset(db,wildcards)
            if len(domains) > 0:
                delete_domain(db,domains)
            arcpy.AddMessage("Copy datasets success!")

        # Start all services connecting to the target workspace
        if services == "ALL":
            start_stop_service(server_name,user_name,password,folder_name,all=True,action="START")
        else:
            for service in services:
                start_stop_service(server_name,user_name,password,folder_name,all=False,service_name=service,action="START")
        arcpy.AddMessage("Restarted services...")

        # Allow database connection
        allow_database_connection(db,allow=True)

        arcpy.AddMessage("Done!")

        return
    except Exception,ex:
        if services == "ALL":
            start_stop_service(server_name,user_name,password,folder_name,all=True,action="START")
        else:
            services = services.split(",")
            for service in services:
                start_stop_service(server_name,user_name,password,folder_name,all=False,service_name=service,action="START")
        arcpy.AddMessage(traceback.format_exc())


if __name__ == "__main__":
    delete()