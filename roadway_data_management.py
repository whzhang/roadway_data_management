import os
import arcpy
import httplib
import urllib
import json
import traceback


def copy_dataset(workspace_from,workspace_to,wildcards):
    try:
        arcpy.env.workspace = workspace_from
        arcpy.env.overwriteOutput = True

        feature_classes = []
        tables = []
        datasets_to_migrate = []

        # copy all datasets if no wildcard is specified.
        if len(wildcards)==0:
            fcs = arcpy.ListFeatureClasses()
            for fc in fcs:
                if not (fc in feature_classes):
                    feature_classes.append(fc)
                    datasets_to_migrate.append(dict(Name=fc,DataType="FeatureClass"))

            tbls = arcpy.ListTables()
            for tbl in tbls:
                if not (tbl in tables):
                    tables.append(tbl)
                    datasets_to_migrate.append(dict(Name=tbl,DataType="Table"))
        else:
            for wildcard in wildcards:
                wildcard ="*"+wildcard+"*"

                fcs = arcpy.ListFeatureClasses(wildcard)
                for fc in fcs:
                    if not (fc in feature_classes):
                        feature_classes.append(fc)
                        datasets_to_migrate.append(dict(Name=fc,DataType="FeatureClass"))

                tbls = arcpy.ListTables(wildcard)
                for tbl in tbls:
                    if not (tbl in tables):
                        tables.append(tbl)
                        datasets_to_migrate.append(dict(Name=tbl,DataType="Table"))

        for dataset in datasets_to_migrate:
            fullname = arcpy.ParseTableName(dataset["Name"], workspace_from)
            database, owner, in_fc_name = fullname.split(",")

            out_fc_name = os.path.join(workspace_to,arcpy.ValidateTableName(in_fc_name, workspace_to))

            arcpy.AddMessage(dataset["Name"]+"-"+dataset["DataType"])
            arcpy.AddMessage("Output: "+out_fc_name)
            if not arcpy.Exists(out_fc_name):
                arcpy.Copy_management(dataset["Name"], out_fc_name, dataset["DataType"])

        return True
    except Exception, ex:
        arcpy.AddMessage(traceback.format_exc())
        return False

def delete_dataset(workspace,wildcards):
    try:
        # return without deleting anything if no wildcard is specified
        if len(wildcards)==0:
            return

        arcpy.env.workspace = workspace
        arcpy.env.overwriteOutput = True

        feature_classes = []
        tables = []
        datasets_to_delete = []

        for wildcard in wildcards:
            wildcard ="*"+wildcard+"*"

            fcs = arcpy.ListFeatureClasses(wildcard)
            for fc in fcs:
                if not (fc in feature_classes):
                    feature_classes.append(fc)
                    datasets_to_delete.append(dict(Name=fc,DataType="FeatureClass"))

            tbls = arcpy.ListTables(wildcard)
            for tbl in tbls:
                if not (tbl in tables):
                    tables.append(tbl)
                    datasets_to_delete.append(dict(Name=tbl,DataType="Table"))

        for dataset in datasets_to_delete:
            if arcpy.Exists(dataset["Name"]):
                arcpy.Delete_management(os.path.join(workspace,dataset["Name"]), dataset["DataType"])

        return True
    except Exception, ex:
        arcpy.AddMessage(traceback.format_exc())
        return False

def delete_domain(workspace,domains):
    try:
        for domain in domains:
            arcpy.DeleteDomain_management(workspace, domain)

        return True
    except Exception as ex:
        arcpy.AddMessage(traceback.format_exc())
        return False

def disconnect_user(sde_workspace, user_name="", all=False):
    try:
        #-------------------------------------------
        # List all users connecting to the database
        arcpy.AddMessage("Users connecting to the database:")

        connected_users = arcpy.ListUsers(sde_workspace)

        for connected_user in connected_users:
            arcpy.AddMessage("Username: {0}, ClientName: {1}, UserID: {2}".format(connected_user.Name,
                                                                                  connected_user.ClientName,
                                                                                  connected_user.ID))
        #-------------------------------------------

        #-------------------------------------------
        # Disconnect users
        if all:
            arcpy.DisconnectUser(sde_workspace, "ALL")
        else:
            for connected_user in connected_users:
                if connected_user.ClientName == user_name:
                    connected_user_id = connected_user.ID
                    arcpy.DisconnectUser(sde_workspace, connected_user_id)
        #-------------------------------------------

        #-------------------------------------------
        # List all users connecting to the database
        # arcpy.AddMessage("Users connecting to the database:")
        #
        # connected_users = arcpy.ListUsers(sde_workspace)
        #
        # for connected_user in connected_users:
        #     arcpy.AddMessage("Username: {0}, ClientName: {1}, UserID: {2}".format(connected_user.Name,
        #                                                                           connected_user.ClientName,
        #                                                                           connected_user.ID))
        #-------------------------------------------
        return True
    except Exception, ex:
        arcpy.AddMessage(traceback.format_exc())
        return False

def allow_database_connection(sde_workspace, allow=True):
    try:
        arcpy.AcceptConnections(sde_workspace,allow)
        return True
    except Exception, ex:
        arcpy.AddMessage("Failed to implement AcceptConnections function. Please connect the geodatabase as an administrator...")
        arcpy.AddMessage(traceback.format_exc())
        return False

def start_stop_service(server_name, user_name, password, folder, all=False, service_name='', action=''):
    try:
        arcpy.AddMessage("Begin to "+action+" '"+service_name+"' service(s) in '"+folder+"' folder on '"+server_name+"' server.")

        server_port = 6080

        if str.upper(action)!="START" and str.upper(action)!="STOP":
            arcpy.AddMessage("Failed to execute. Invalid START/STOP service action.")
            return False

        # Get a token
        token = get_token(server_name, server_port, user_name, password)
        if token == "":
            arcpy.AddMessage("Could not generate a token with the username and password provided.")
            return False

        # Construct URL to read folder
        if str.upper(folder) == "ROOT":
            folder = ""
        else:
            folder += "/"

        folder_url = "/arcgis/admin/services/" + folder

        # This request only needs the token and the response formatting parameter
        params = urllib.urlencode({'token': token, 'f': 'json'})
        headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}

        # Connect to URL and post parameters
        http_conn = httplib.HTTPConnection(server_name, server_port)
        http_conn.request("POST", folder_url, params, headers)

        # Read response
        response = http_conn.getresponse()
        if (response.status != 200):
            http_conn.close()
            arcpy.AddMessage("Could not read folder information.")
            return False
        else:
            data = response.read()

            # Check that data returned is not an error object
            if not assert_json_success(data):
                arcpy.AddMessage("Error when reading folder information. " + str(data))
                return False
            else:
                arcpy.AddMessage("Processed folder information successfully. Now processing services...")

        # Deserialize response into Python object
        dataObj = json.loads(data)
        http_conn.close()

        # Start/Stop all services in the folder is 'all' is set to true, otherwise, start/stop the specified service
        if all:
            # Loop through each service in the folder and stop or start it
            for item in dataObj['services']:
                full_svc_name = item['serviceName'] + "." + item['type']

                # Construct URL to stop or start service, then make the request
                url = "/arcgis/admin/services/" + folder + full_svc_name + "/" + str.upper(action)
                http_conn.request("POST", url, params, headers)

                # Read stop or start response
                response = http_conn.getresponse()
                if (response.status != 200):
                    http_conn.close()
                    arcpy.AddMessage("Error while executing " + action + ". Please check the URL and try again.")
                    return False
                else:
                    data = response.read()

                    # Check that data returned is not an error object
                    if not assert_json_success(data):
                        arcpy.AddMessage("Error returned when trying to " + action + " service " + full_svc_name + ".")
                        arcpy.AddMessage(str(data))
                    else:
                        arcpy.AddMessage("Service " + full_svc_name + " processed successfully.")

            http_conn.close()
        else:
            full_svc_name = ""
            for item in dataObj['services']:
                if item['serviceName'] == service_name:
                    full_svc_name = item['serviceName'] + "." + item['type']
                    continue

            if full_svc_name=="":
                arcpy.AddMessage("Service '" + server_name + "' not exist.")
                return False

            # Construct URL to stop or start service, then make the request
            url = "/arcgis/admin/services/" + folder + full_svc_name + "/" + str.upper(action)
            http_conn.request("POST", url, params, headers)

            # Read stop or start response
            response = http_conn.getresponse()
            if (response.status != 200):
                http_conn.close()
                arcpy.AddMessage("Error while executing" + action + ". Please check the URL and try again.")
                return False
            else:
                data = response.read()

                # Check that data returned is not an error object
                if not assert_json_success(data):
                    arcpy.AddMessage("Error returned when trying to " + action + " service " + full_svc_name + ".")
                    arcpy.AddMessage(str(data))
                else:
                    arcpy.AddMessage("Service " + full_svc_name + " processed successfully.")

            http_conn.close()

        return True
    except Exception, ex:
        arcpy.AddMessage(traceback.format_exc())
        return False

# A function to generate a token given username, password and the adminURL.
def get_token(server_name, server_port, username, password):
    try:
        # Token URL is typically http://server[:port]/arcgis/admin/generateToken
        tokenURL = "/arcgis/admin/generateToken"
        params = urllib.urlencode({'username': username, 'password': password, 'client': 'requestip', 'f': 'json'})
        headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}

        # Connect to URL and post parameters
        httpConn = httplib.HTTPConnection(server_name, server_port)
        httpConn.request("POST", tokenURL, params, headers)

        # Read response
        response = httpConn.getresponse()
        if (response.status != 200):
            httpConn.close()
            arcpy.AddMessage("Error while fetching tokens from admin URL. Please check the URL and try again.")
            return
        else:
            data = response.read()
            httpConn.close()

            # Check that data returned is not an error object
            if not assert_json_success(data):
                return

            # Extract the token from it
            token = json.loads(data)
            return token['token']
    except Exception, ex:
        arcpy.AddMessage(traceback.format_exc())

def assert_json_success(data):
    obj = json.loads(data)
    if 'status' in obj and obj['status'] == "error":
        arcpy.AddMessage("Error: JSON object returns an error. " + str(obj))
        return False
    else:
        return True