import arcpy
import traceback
import default_parameters
from roadway_data_management import copy_dataset,start_stop_service,disconnect_user,allow_database_connection

def copy():
    # input parameters
    source_db = default_parameters.SOURCE_DB
    target_db = default_parameters.TARGET_DB
    wildcards = str(arcpy.GetParameter(0))
    server_name = str(arcpy.GetParameter(1))
    user_name = str(arcpy.GetParameter(2))
    password = str(arcpy.GetParameter(3))
    folder_name = str(arcpy.GetParameter(4))
    services = str(arcpy.GetParameter(5))

    try:
        # parse user input wildcards
        wildcards = wildcards.split(",")

        # Stop all services connecting to the target workspace
        if services == "ALL":
            if not start_stop_service(server_name,user_name,password,folder_name,all=True,action="STOP"):
                return
        else:
            services = services.split(",")
            for service in services:
                if not start_stop_service(server_name,user_name,password,folder_name,all=False,service_name=service,action="STOP"):
                    return
        arcpy.AddMessage("Stopped services...")

        # disconnect all users connecting to the target workspace and disallow database connection
        if allow_database_connection(target_db,allow=False) and disconnect_user(target_db,all=True):
            # migrate datasets
            copy_dataset(source_db,target_db,wildcards)
            arcpy.AddMessage("Copy datasets success!")

        # Start all services connecting to the target workspace
        if services == "ALL":
            start_stop_service(server_name,user_name,password,folder_name,all=True,action="START")
        else:
            for service in services:
                start_stop_service(server_name,user_name,password,folder_name,all=False,service_name=service,action="START")
        arcpy.AddMessage("Restarted services...")

        # Allow database connection
        allow_database_connection(target_db,allow=False)

        arcpy.AddMessage("Done!")

        return
    except Exception,ex:
        if services == "ALL":
            start_stop_service(server_name,user_name,password,folder_name,all=True,action="START")
        else:
            for service in services:
                start_stop_service(server_name,user_name,password,folder_name,all=False,service_name=service,action="START")
        arcpy.AddMessage(traceback.format_exc())


if __name__ == "__main__":
    copy()